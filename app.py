from flask import Flask, request, jsonify, make_response, send_from_directory, render_template
from werkzeug.utils import secure_filename
from flask_pymongo import PyMongo

import datetime
from bson import ObjectId
import json
import os
import pandas as pd

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLD = 'uploaded_files/'
UPLOAD_FOLDER = os.path.join(APP_ROOT, UPLOAD_FOLD)
ALLOWED_EXTENSIONS = {'csv'}

DBNAME = 'flaskCRM'
app = Flask(__name__)
app.config['MONGO_URI'] = 'mongodb://127.0.0.1:27017/' + DBNAME
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

mongo = PyMongo(app)


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


def convert_date(date):
    date_array = date.split('/')

    day = date_array[1]
    month = date_array[0]
    year = date_array[2]

    return datetime.datetime(int(year), int(month), int(day), 0, 0, 0)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def import_content(filepath, filename):
    collection_name = filename
    db_cm = mongo.db[collection_name]
    db_cm.remove()
    db_cm.create_index([('Customer', 1)], unique=False)
    db_cm.create_index([('Date received', 1)], unique=False)
    db_cm.create_index([('Date sent to company', 1)], unique=False)

    try:
        csv_dir = os.path.dirname(__file__)
        file_res = os.path.join(csv_dir, filepath)
        data = pd.read_csv(file_res)
        data_json = json.loads(data.to_json(orient='records'))

        for obj in data_json:
            obj["Date received"] = convert_date(obj["Date received"])
            obj["Date sent to company"] = convert_date(obj["Date sent to company"])

        db_cm.insert(data_json)

    except Exception as err:
        print(err)
        return False
    return True


def get_data_with_filters(data_source, filter, fields):
    x = []
    cur = mongo.db[data_source].find(filter, fields)
    print(cur)
    for i in cur:
        x.append(i)
    return x


def get_banks(data_source):
    field = "Customer"
    data = mongo.db[data_source].aggregate([
        {
            "$group": {
                "_id": {
                    field: "$" + field
                }
            }
        },
        {
            "$sort": {
                "_id." + field: 1
            }
        }
    ])
    x = []
    for i in data:
        x.append(i["_id"]["Customer"])
    return x


def get_top_three_banks(data_source):
    field = "Customer"
    data = mongo.db[data_source].aggregate([
        {
            "$group": {
                "_id": {
                    field: "$" + field,
                },
                "count": {"$sum": 1}
            }
        },
        {
            "$sort": {
                "count": -1
            }
        },
        {"$limit": 3},
    ])
    x = []
    for i in data:
        x.append({"customer": i["_id"][field], "count": i["count"]})
    return x


def get_top_ten_banks(data_source, period):
    field = "Customer"
    if period == 'Q1':
        gteDate = 1
        lteDate = 3
    elif period == 'Q2':
        gteDate = 4
        lteDate = 6
    elif period == 'Q3':
        gteDate = 7
        lteDate = 9
    elif period == 'Q4':
        gteDate = 10
        lteDate = 12
    else:
        gteDate = 1
        lteDate = 12
    data = mongo.db[data_source].aggregate([
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                {"$month": "$Date received"},
                                gteDate
                            ]
                        },
                        {
                            "$lte": [
                                {"$month": "$Date received"},
                                lteDate
                            ]
                        }
                    ]
                }
            }
        },
        {
            "$group": {
                "_id": {
                    field: "$" + field,
                },
                "count": {"$sum": 1}
            }
        },
        {
            "$sort": {
                "count": -1
            }
        },
        {"$limit": 10}
    ])
    x = []
    for i in data:
        x.append({"customer": i["_id"][field], "count": i["count"]})
    return x


def get_data_source_list():
    return mongo.db.collection_names()


def get_complains_record_numbers(data_source):
    return str(mongo.db[data_source].count())


@app.route("/")
def main():
    selected_bank = request.args.get("bank")
    selected_percentage = request.args.get("percentage")
    selected_period = request.args.get("period")
    selected_data_source = request.args.get("data_source")

    data_sources = get_data_source_list()
    tabs_data = {}
    for data_source in data_sources:
        tabs_data[data_source] = {
            "banks": get_top_three_banks(data_source),
            "total": get_complains_record_numbers(data_source)
        }
    if selected_data_source:
        banks = get_banks(selected_data_source)
        chart_data = get_top_ten_banks(selected_data_source, selected_period)
    else:
        banks = []
        top_three = []
        chart_data = []

    percentages = ['25%', '50%', '75%', '100%']
    periods = ['Q1', 'Q2', 'Q3', 'Q4']

    return render_template("Main.html",
                           percentages=percentages,
                           banks=banks,
                           periods=periods,
                           data_sources=data_sources,
                           selected_bank=selected_bank,
                           selected_percentage=selected_percentage,
                           selected_period=selected_period,
                           selected_data_source=selected_data_source,
                           tabs_data=tabs_data,
                           chart_data=chart_data
                           )


@app.route("/upload", methods=['POST', 'GET'])
def upload():
    if request.method == 'POST':
        file = request.files['file']

        if file and allowed_file(file.filename):
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(file.filename))
            file.save(file_path)

            import_result = import_content(file_path, file.filename.rsplit('.', 1)[0].lower())
            if not import_result:
                response_body = {"message": "file cannot be imported", "summary": ''}
                return make_response(jsonify(response_body), 400)

            response_body = {"message": "file uploaded successfully.", "summary": ''}
            return make_response("<script>window.location.replace('/');</script>", 200)

    response_body = {"message": "unsuccessful upload.", "summary": ''}
    return make_response(jsonify(response_body), 400)


@app.route("/api/collections_list", methods=['POST', 'GET'])
def collections_names():
    response_body = {"message": "200", "data": get_data_source_list(), "summary": ''}
    return make_response(jsonify(response_body), 200)


@app.route("/api/filtered_data", methods=['POST', 'GET'])
def filtered_data():
    if request.method == 'GET':
        return get_collection_record_numbers("source-vendor")


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
